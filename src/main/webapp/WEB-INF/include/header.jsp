<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Grzegorz Świdnicki">
    <title></title>
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/bootstrap/bootstrap.min.css"/>
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/style.css"/>
    <script src="${pageContext.servletContext.contextPath}/resources/js/bootstrap/bootstrap.min.js"></script>
    <script src="${pageContext.servletContext.contextPath}/resources/js/jquery/jquery-2.2.4.min.js"></script>
    <script src="${pageContext.servletContext.contextPath}/resources/js/main.js"></script>
</head>
<body>